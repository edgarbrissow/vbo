#ifndef UNI_FACE_H
#define UNI_FACE_H

namespace uni{
  class Face{
  public:
    Face(unsigned v1, unsigned v2, unsigned v3);
    virtual ~Face();
    unsigned getV1();
    unsigned getV2();
    unsigned getV3();
    int * parse();
    
    void print();
    
  private:
    unsigned _v1;
    unsigned _v2;
    unsigned _v3;
    
  };
}// end

#endif