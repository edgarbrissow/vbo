#ifndef UNI_INITMANAGER_H
#define UNI_INITMANAGER_H


namespace uni {
class InitManager{
public:
  virtual ~InitManager();
  
  static void initSDLVideo(unsigned w, unsigned h);
  static void initGL();
  static void initView(unsigned w, unsigned h);
  static void initGlew();
  
  
private:
  InitManager();
  
  
};  
    
}

#endif