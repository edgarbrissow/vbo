#ifndef UNI_MESH_H
#define UNI_MESH_H
#include "vertex.h"
#include "face.h"
#include <vector>

namespace uni{
  class Mesh{
  public:
    Mesh();
    virtual ~Mesh();
    unsigned addVertex(Vertex vertex);
    bool conect(unsigned a, unsigned b, unsigned c);
    unsigned vertexSize();
    std::vector<float> getArrayPos();
    std::vector<int> getArrayIndex();
    unsigned faceSize();
    Vertex getVertex(unsigned index);
    Face getFace(unsigned index);
  private:
    std::vector<Vertex> _vertexs;
    std::vector<Face> _faces;
  };
}




#endif