#ifndef UNI_OBJ_H
#define UNI_OBJ_H
#include "mesh.h"
#include <string>
#include <fstream>

namespace uni{
  class Obj{
  public:
    static Mesh readObjToMesh(std::string file);
    static std::string loadFromFile(const std::string path);
    
  private:
    std::ifstream file;
    std::string file_name;
    unsigned index;  

    
  };
}



#endif