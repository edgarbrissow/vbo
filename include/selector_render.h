
#ifndef UNI_SELECTOR_RENDER_H_
#define UNI_SELECTOR_RENDER_H_

#include "render.h"
#include "vec3.h"

namespace uni {

struct Selector {
    Vec3 v[3];
    unsigned i[3];
};

class SelectorRender : public Render {
public:
    SelectorRender(const Selector& selector);
    virtual ~SelectorRender();
    void recreate(const Selector& selector );
};

} //END namespace

#endif
