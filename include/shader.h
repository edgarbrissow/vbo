#ifndef UNI_SHADER
#define UNI_SHADER

#include <string>

namespace uni{
  class Shader{
  public:
    Shader();
    ~Shader();
    void vertex(std::string code);
    void fragment(std::string code);
    void build();
    void enable(bool status);
    std::string shaderLog(unsigned id);
    std::string programLog();
    
  private:
    unsigned _id;
    unsigned _vID;
    unsigned _fID;
    
  };
}
#endif