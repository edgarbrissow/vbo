#ifndef UNI_TRIANGLE_MESH_H_
#define UNI_TRIANGLE_MESH_H_

#include "render.h"
#include "mesh.h"

namespace uni {

class TriangleMeshRender : public Render {
public:
    TriangleMeshRender(Mesh& mesh);
    virtual ~TriangleMeshRender();

    void addSubMesh(Mesh& subMesh, int* gambi);

private:
    void init(Mesh& mesh);
    Mesh meshdasgambiarra;
};

} //END namespace

#endif
