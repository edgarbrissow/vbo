#ifndef UNI_VBO_H
#define UNI_VBO_H

#include <SDL/SDL.h>
#include <vector>
#include <mesh.h>
#include "render.h"

namespace uni {
  class VBO{
  public:
    VBO(unsigned w, unsigned h);
    virtual ~VBO();

  private:
    void start();
    void mainLoop();
    void checkEvent(SDL_Event& e);
    void draw();
    void Vbo();
    void drawVBO();
    void updateSelector(bool more);
    void extrude();


  private:
    unsigned _w;
    unsigned _h;
    bool done;
    float _x;
    float _y;
    float _rotation;
    unsigned _indexVB;
    unsigned _colorVB;
    unsigned _positionVB;
    Mesh _m;
    unsigned indice_selector;
    unsigned max_selector;
    std::vector<int> ind;
    std::vector<float> ind2;
    std::vector<Render> render;
    
  };
} //END namespace



#endif
