#ifndef UNI_VEC3_H
#define UNI_VEC3_H
namespace uni{
  class Vec3{
  public:
    Vec3();
    Vec3(float x, float y, float z);
    virtual ~Vec3();
    void setX(float x);
    void setY(float y);
    void setZ(float z);
    float getX() const;
    float getY() const;
    float getZ() const;
    
    Vec3 operator * (float scalar) const;
    Vec3 operator +(const Vec3& other) const;
    Vec3 operator - (const Vec3& other) const;
//     void operator = (const Vec3& other) const;
    Vec3 cross(const Vec3& other) const;
    Vec3 normalize() const;
    
    void print() const;
    
  private:
    float _x;
    float _y;
    float _z;
  };
} // end'
#endif