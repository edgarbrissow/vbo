#ifndef UNI_VERTEX_H
#define UNI_VERTEX_H
#include "vec3.h"

namespace uni{
  class Vertex{
  public:
    Vertex();
    virtual ~Vertex();
    Vec3 getPos();
    Vec3 getNormal();
    void setPos(Vec3 pos);
    void setNormal(Vec3 normal);
    
    bool operator==(const Vertex& other);
  private:
    Vec3 _pos;
    Vec3 _normal;
  };
}

#endif