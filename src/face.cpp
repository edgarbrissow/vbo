#include "face.h"
#include <iostream>

namespace uni{
Face::Face(unsigned int v1, unsigned int v2, unsigned int v3)
:_v1(v1), _v2(v2), _v3(v3){}

unsigned int Face::getV1()
{
  return _v1;
}

unsigned int Face::getV2()
{
  return _v2;
}

unsigned int Face::getV3()
{
  return _v3;
}

void Face::print()
{
  std::cout << "F1: " << _v1 << " F2: " << _v2 << " F3: " << _v3 << std::endl;
}

int* Face::parse()
{
  int vec[3] = {_v1, _v2, _v3};
  return vec;
}



Face::~Face(){}





  
  
  
}





