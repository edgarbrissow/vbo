#include "init_manager.h"
#include <GL/glew.h>
#include <SDL/SDL.h>
#include <iostream>


namespace uni{

void InitManager::initSDLVideo(unsigned int w, unsigned int h){
  SDL_Init(SDL_INIT_EVERYTHING);
  int flags = SDL_OPENGL;
  flags |= SDL_GL_DOUBLEBUFFER;
  flags |= SDL_HWPALETTE;
  flags |= SDL_RESIZABLE;
  flags |= SDL_SWSURFACE;
  
  const SDL_VideoInfo* videoInfo = SDL_GetVideoInfo();
  
  if(videoInfo->blit_hw) flags |= SDL_HWACCEL;
  
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  
  static const int bits = 16;
  SDL_SetVideoMode(w, h, bits, flags);
  
  static const int delay = 10;
  static const int interval = 100;
  SDL_EnableKeyRepeat(delay, interval);
}


void InitManager::initGL(){
    glClearColor(0.f, 0.f, 0.f, 1.f);
    glClearDepth(1.f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void InitManager::initView(unsigned int w, unsigned int h){
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  
  static const float angle = 45.f;
  static const float ratio = float(w)/float(h);
  static const float nearPlane = .1f;
  static const float farPlane = 1000.f;
  gluPerspective(angle, ratio, nearPlane, farPlane);
  
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void InitManager::initGlew(){
  glewExperimental=true;
  GLenum err = glewInit();
  
  if(err!= GLEW_OK)
    std::cout << "ERROR?" << err << std::endl;
}


  
InitManager::InitManager(){};
InitManager::~InitManager(){};

}//END namespace