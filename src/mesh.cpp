#include "mesh.h"
#include <iostream>

namespace uni{
Mesh::Mesh(){}
Mesh::~Mesh(){}

unsigned int Mesh::addVertex(Vertex vertex){
  
  for(int i=0; i<_vertexs.size(); i++){
    if(_vertexs.at(i) == vertex)
      return i;
  }
  
  
  _vertexs.push_back(vertex);
  return _vertexs.size()-1;
}

unsigned int Mesh::vertexSize()
{
  return _vertexs.size();
}

unsigned int Mesh::faceSize()
{
  return _faces.size();
}


bool Mesh::conect(unsigned int a, unsigned int b, unsigned int c)
{
  Face f(a,b,c);
  _faces.push_back(f);
}

std::vector<float> Mesh::getArrayPos()
{
  std::vector<float> vec;
  
  std::cout << _vertexs.size() << std::endl;

  for(int f = 0; f < _vertexs.size(); f++){
      vec.push_back(_vertexs.at(f).getPos().getX());
      vec.push_back(_vertexs.at(f).getPos().getY());
      vec.push_back(_vertexs.at(f).getPos().getZ());
  }
  
  return vec;
}


std::vector< int > Mesh::getArrayIndex()
{
  std::vector<int> fa;
  std::cout << _faces.size() << std::endl;
  
  for(int f=0; f < _faces.size(); f++){
      fa.push_back(_faces.at(f).getV1());
      fa.push_back(_faces.at(f).getV2());
      fa.push_back(_faces.at(f).getV3());
      
  }
  
  return fa;
  
}
Face Mesh::getFace(unsigned int index)
{
    return _faces.at(index);
}

Vertex Mesh::getVertex(unsigned int index)
{
  return _vertexs.at(index);
}



}


