#include "obj.h"
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include "face.h"
#include "mesh.h"
#include "vec3.h"
#include "vertex.h"
namespace uni{
  
  
Mesh Obj::readObjToMesh(std::string file_str){
  
   char temp_char;
  float x, y, z;
  unsigned int v1,v2,v3,vn1, vn2, vn3;
//   Vec3 temp_vec(0.0,0.0,0.0);
//   Face temp_face(0,0,0);
  std::string temp_string;
  std::stringstream ss;
  std::fstream file;
  
  file.open(file_str.c_str(), std::ios::in);
  if(file.is_open())
    std::cout << "Abriu o arquivo" << std::endl;
  else
      std::cout << "Não abriu" << std::endl;
  Mesh mesh;
  unsigned count = 0;
  
  
   std::vector<Vec3> pos;
   std::vector<Vec3> normal;
  
  while(std::getline(file,temp_string)){
   ss.str(std::string()); 
   ss.clear();
   ss << temp_string;
   
   if(temp_string[0] == 'v' && temp_string[1] == 'n'){
      ss >> temp_char >> temp_char >> x >> y >> z;
     Vec3 n;
     n.setX(x);
     n.setY(y);
     n.setZ(z);
     
     normal.push_back(n);
  }
   
   if(temp_string[0] == 'v' && temp_string[1] == ' '){
    ss >> temp_char >> x >> y >> z;
    Vec3 p;
    p.setX(x);
    p.setY(y);
    p.setZ(z);
    
    pos.push_back(p);
   }
   else{ 
     if(temp_string[0] == 'f' && temp_string[1] == ' '){
      //Do stuff
      ss >> temp_char >> v1 >> temp_char >> temp_char >> vn1
	 >> v2 >> temp_char >> temp_char >> vn2
	 >> v3 >> temp_char >> temp_char >> vn3;
      
	Vertex vx1;
	Vertex vx2;
	Vertex vx3;
	vx1.setPos(pos.at(v1-1));
	vx1.setNormal(normal.at(vn1-1));
	vx2.setPos(pos.at(v2-1));
	vx2.setNormal(normal.at(vn2-1));
	vx3.setPos(pos.at(v3-1));
	vx3.setNormal(normal.at(vn3-1));
	
	mesh.conect(mesh.addVertex(vx1), mesh.addVertex(vx2), mesh.addVertex(vx3));

    }
   } 
  }
  file.close();
  std::cout << "Terminou de ler o arquivo" << std::endl;
  
  return mesh;
}


std::string Obj::loadFromFile(const std::string path){
  std::fstream file;
   file.open(path.c_str(), std::ios::in);
   std::string str;
   file.seekg(0,std::ios::end);
   str.reserve(file.tellg());
   file.seekg(0, std::ios::beg);
   str.assign((std::istreambuf_iterator<char>(file)), 
	      std::istreambuf_iterator<char>());
   return str;
   
}

  
  
  
}