#include "render.h"
#include <iostream>
#include <cstring>


namespace uni{
  
  
  const unsigned Render::_renderModes[] = {GL_TRIANGLES, GL_LINE_LOOP, GL_POINT};
  
  void Render::draw(){
    
    glBindBuffer(GL_ARRAY_BUFFER, _positionVB);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, 0);
    
    glEnableClientState(GL_ELEMENT_ARRAY_BUFFER);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indiceVB);
    
    glDrawElements(_renderModes[_renderMode],  _indicesSize, GL_UNSIGNED_INT, 0);
    
    glDisableClientState(GL_ELEMENT_ARRAY_BUFFER);
    glDisableClientState(GL_VERTEX_ARRAY);
  }
  
  Render::Render(){}
  Render::~Render(){}
  
  void Render::rePosition(float* position, unsigned int arraySize){
    
    glBindBuffer(GL_ARRAY_BUFFER, _positionVB);
    
    void* vertexPtr = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    
    memcpy(vertexPtr, (float*) position, arraySize * sizeof(float));
    
    glUnmapBuffer(GL_ARRAY_BUFFER);
    
    
  }
  
  void Render::print()
  {
    std::cout << "INDICE VBO: " << _indiceVB << std::endl
    << "INDICE SIZE: " << _indicesSize << std::endl
    << "POSITION VBO" << _positionVB << std::endl;
  }
  
  
  
  void Render::initIndices(unsigned int* indices, unsigned int indicesSize)
  {
    glGenBuffers(1, indices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *indices);
//     indices = &_indiceVB;
    _indicesSize = indicesSize;
    //     glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSize * sizeof(unsigned), indices, GL_STATIC_DRAW);
    
  }
  
  void Render::initPosition(unsigned* position, unsigned int arraySize)
  {
    glGenBuffers(1, position);
    glBindBuffer(GL_ARRAY_BUFFER, *position);
    //     position = &_positionVB;
    
    //     glBufferData(GL_ELEMENT_ARRAY_BUFFER, arraySize * sizeof(float), position, GL_STATIC_DRAW);
  }
  
  void Render::pushIndex(unsigned int size, unsigned int* data)
  {
    int bufferSize;
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size * sizeof(unsigned), data, GL_STATIC_DRAW);  
    glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, 

                       GL_BUFFER_SIZE, 

                       &bufferSize);
    
    std::cout << "ELEMENTS SIZE: " << bufferSize << std::endl;
  }
  
  void Render::pushData(unsigned int size, unsigned int& vboIdx, float* data)
  {
    int bufferSize;
    glBufferData(GL_ARRAY_BUFFER, size * sizeof(float), data, GL_STATIC_DRAW);
        glGetBufferParameteriv(GL_ARRAY_BUFFER, 

                       GL_BUFFER_SIZE, 

                       &bufferSize);
    
    std::cout << "BUFFER SIZE: " << bufferSize << std::endl;
    
    
  }
  
  
  
  
  
  
  
  
  
}//END

