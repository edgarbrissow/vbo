#include "selector_render.h"

namespace uni{
  
SelectorRender::SelectorRender(const Selector& selector)
{
  unsigned _positions;
  unsigned vbo;
  float pos[9];
  unsigned ind[3] = {selector.i[0], selector.i[1], selector.i[2]};
  
  for(int i=0; i < 3; i ++){
    
    pos [ i * 3 ] = selector.v[i].getX();
    pos [ i * 3 + 1 ] = selector.v[i].getY();
    pos[ i * 3 + 2]  = selector.v[i].getZ();
  }
  
  this->_renderMode = LINE_MODE;
  this->initIndices(&_indiceVB, 3);
  this->initPosition(&_positionVB, 3);
  this->pushIndex(3, ind);
  this->pushData(9,vbo, pos );
  
  
}

void SelectorRender::recreate(const Selector& selector)
{
      float _positions;
  unsigned vbo;
  float pos[9];
  unsigned ind[3] = {selector.i[0], selector.i[1], selector.i[2]};
  
  for(int i=0; i < 3; i ++){
    
    pos [ i * 3 ] = selector.v[i].getX();
    pos [ i * 3 + 1 ] = selector.v[i].getY();
    pos[ i * 3 + 2]  = selector.v[i].getZ();
  }
    this->rePosition(pos, 9);

}

SelectorRender::~SelectorRender()
{
  
}

  
  
  
}