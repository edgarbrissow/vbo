#include "shader.h"
#include "GL/glew.h"
namespace uni{
  
Shader::Shader(){
  _id = glCreateProgram();
  glAttachShader(_id, _vID);
  glAttachShader(_id, _fID);
  glLinkProgram(_id);
}

Shader::~Shader(){
  glDetachShader(_id, _vID);
  glDetachShader(_id, _fID);
  glDeleteShader(_vID);
  glDeleteShader(_fID);
  glDeleteProgram(_id);
}

void Shader::vertex(std::string code){
  const char* vert = code.c_str();
  glShaderSource(_vID, 1, &vert, 0);
}

void Shader::fragment(std::string code){
  const char* vert = code.c_str();
  glShaderSource(_fID, 1, &vert, 0);
}

void Shader::build(){
  glCompileShader(_vID);
  glCompileShader(_fID);
}

void Shader::enable(bool status){
  if(status)
    glUseProgram(_id);
  else
    glUseProgram(0);
}

std::string Shader::shaderLog(unsigned int id){
  //TODO later
}

std::string Shader::programLog(){
  //TODO later
}
  
} //end namespace