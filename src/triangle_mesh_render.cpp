#include "triangle_mesh_render.h"
#include "obj.h"
#include <vector>
#include <iostream>
namespace uni{
 
TriangleMeshRender::TriangleMeshRender( Mesh& mesh){
  this->_renderMode = TRIANGLE_MODE;
  init(mesh);
}

TriangleMeshRender::~TriangleMeshRender(){}


void TriangleMeshRender::addSubMesh( Mesh& subMesh, int* gambi)
{
  
    unsigned aux;
    int bufferSize;
  
      std::vector<float> pos = subMesh.getArrayPos();
      std::vector<int> ind = subMesh.getArrayIndex();
      //Cria um novo vbo
    glGenBuffers(1, &aux);
    // Liga o vbo original ao GL_COPY_READ_BUFFER
    glBindBuffer(GL_COPY_READ_BUFFER, _positionVB);
    //Pega o tamanho de GL_COPY_READ_BUFFER
    glGetBufferParameteriv(GL_COPY_READ_BUFFER, GL_BUFFER_SIZE, &bufferSize);
    
    //Liga o novo vbo a GL_COPY_WRITE_BUFFER
    glBindBuffer(GL_COPY_WRITE_BUFFER, aux);
    //Aloca memoria para o GL_COPY_WRITE_BUFFER de bufferSize + 9* float
    glBufferData(GL_COPY_WRITE_BUFFER, bufferSize +( 9*sizeof(float)), 0, GL_STATIC_DRAW);
    //Copia o GL_COPY_READ_BUFFER para o GL_COPY_WRITE_BUFFER no inicio com o tamanho de bufferSize
    glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, bufferSize);
    //Deleta a vbo original
    glDeleteBuffers(1, &_positionVB);
    
    _indicesSize += 21;
    //O indice da VBO original recebe o indice da nova vbo
    _positionVB = aux;
    // Liga a vbo com um GL_ARRAY_BUFFER
    glBindBuffer(GL_ARRAY_BUFFER, _positionVB);
    // Adiciona no GL_ARRAY_BUFFER o conteudo de pos.data() na posicao bufferSize com o tamanho de 9*float
    glBufferSubData(GL_ARRAY_BUFFER, bufferSize, 9*sizeof(float), pos.data());
    


    glGenBuffers(1, &aux);
    glBindBuffer(GL_COPY_READ_BUFFER, _indiceVB);
    glGetBufferParameteriv(GL_COPY_READ_BUFFER, GL_BUFFER_SIZE, &bufferSize);
    
    glBindBuffer(GL_COPY_WRITE_BUFFER, aux);
    glBufferData(GL_COPY_WRITE_BUFFER, bufferSize +( 21 * sizeof(unsigned)), 0, GL_STATIC_DRAW);
    glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, bufferSize);
    glDeleteBuffers(1, &_indiceVB);
    
//    Face gambi =  meshdasgambiarra->getFace(1);
//    std::cout << "BAM" << meshdasgambiarra.faceSize() << std::endl;
//     int tst[3] = {gambi.getV1(), gambi.getV2(), gambi.getV3()};

    _indiceVB = aux;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indiceVB);
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, bufferSize, 21 * sizeof(unsigned), gambi);
  
  
  
/*  
  int bufferSize;
  unsigned newVBO, vbo, vb2;
  unsigned ps;
  
  int *index = meshdasgambiarra->getFace(meshdasgambiarra->faceSize()-1).parse();
  std::vector<float> pos = subMesh.getArrayPos();
//      this-> initIndices(&vb2, index.size());
//     std::cout << index.size() << std::endl;
//     this->initPosition(&ps, pos.size() );
//     this->pushIndex(index.size(), (unsigned*) index.data()	);
//     this->pushData(pos.size(), vbo,(float*) pos.data());
  

 glGenBuffers(1, &newVBO);
glBindBuffer(GL_COPY_READ_BUFFER, _indiceVB);

glGetBufferParameteriv(GL_COPY_READ_BUFFER, 

                       GL_BUFFER_SIZE, 

                       &bufferSize);

glBindBuffer(GL_COPY_WRITE_BUFFER, newVBO);

std::cout<< "bfsize" << bufferSize << std::endl;


glGetBufferParameteriv(GL_COPY_READ_BUFFER,

                       GL_BUFFER_SIZE, 

                       &bufferSize);

glBufferData(GL_COPY_WRITE_BUFFER, bufferSize +3 , 0, GL_STATIC_DRAW);

std::cout<< "bfsize" << bufferSize << std::endl;
// std::cout<<_indiceVB << std::endl;

glCopyBufferSubData(GL_COPY_READ_BUFFER, 

                    GL_COPY_WRITE_BUFFER, 

                    0, 0, bufferSize);

std::cout<< "bfsize" << bufferSize << std::endl;

glBufferSubData( GL_COPY_WRITE_BUFFER,bufferSize,3*sizeof(unsigned), index); 


// glBindBuffer(GL_COPY_READ_BUFFER, vb2);
// glBindBuffer(GL_COPY_WRITE_BUFFER, newVBO);
// 
// glCopyBufferSubData(GL_COPY_READ_BUFFER, 
// 
//                     GL_COPY_WRITE_BUFFER, 
// 
//                     0, bufferSize, pos.size());


// glBindBuffer(GL_COPY_READ_BUFFER, _positionVB);

// glGetBufferParameteriv(GL_COPY_READ_BUFFER, 

//                        GL_BUFFER_SIZE, 

//                        &bufferSize);

 

// glBindBuffer(GL_COPY_WRITE_BUFFER, newVBO);

// std::cout<< "new vbo" <<_indiceVB << std::endl;

// glBufferData(GL_COPY_WRITE_BUFFER, bufferSize, 0, GL_STATIC_DRAW);

//  std::cout<<_indiceVB << std::endl;

// glCopyBufferSubData(GL_COPY_READ_BUFFER, 

//                     GL_COPY_WRITE_BUFFER, 

//                     0, 0, bufferSize);



glBindBuffer(GL_ARRAY_BUFFER, newVBO);
glBufferSubData(GL_ARRAY_BUFFER,bufferSize,pos.size()*sizeof(float),(float*) pos.data()); 

 _indiceVB = newVBO;
// std::cout<<_indiceVB << std::endl;*/
}




void TriangleMeshRender::init( Mesh& mesh)
{
    std::vector<int> index = mesh.getArrayIndex();
    std::vector<float> position = mesh.getArrayPos();
    unsigned _positions;
    unsigned _indices;
    unsigned vbo;
    
   this-> initIndices(&_indices, index.size());
    std::cout << index.size() << std::endl;
    this->initPosition(&_positions, position.size() );
    this->pushIndex(index.size(), (unsigned*) index.data()	);
    this->pushData(position.size(), vbo,(float*) position.data());
    _indiceVB = _indices;
    _positionVB = _positions;
    
}
}