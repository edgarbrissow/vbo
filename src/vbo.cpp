// #define GL_GLEXT_PROTOTYPES
// #include <GL/glext.h>
#include "vbo.h"
#include "triangle_mesh_render.h"
#include "init_manager.h"
#include <GL/glew.h>
#include <iostream>
#include "mesh.h"
#include "obj.h"
#include "selector_render.h"


namespace uni{
  VBO::VBO(unsigned int w, unsigned int h)
  :_w(w),_h(h), done(false), _rotation(0.0f), _x(-4.5f), _y(0.0f) 
  {
    
    std::cout << "init" << std::endl;
    
    this->start();
    Vbo();

    this->mainLoop();
    
  };
  
  VBO::~VBO(){};
  
  void VBO::start(){
    InitManager::initSDLVideo(_w, _h);
    std::cout << "sdl" << std::endl;
    InitManager::initGL();
    std::cout << "gl" << std::endl;
    InitManager::initView(_w, _h);	
    std::cout << "view" << std::endl;
   InitManager::initGlew();
    std::cout << "glew " << std::endl;
   
    
  }
  
  void VBO::mainLoop(){
      SDL_Event e;
  while(!done){
    this->checkEvent(e);
    this->draw();
  }
    
  }
  
  void VBO::checkEvent(SDL_Event& e){
     while(SDL_PollEvent(&e)){
      switch(e.type){
	case SDL_QUIT:
	  done = true; break;
	
	case SDL_KEYDOWN:
	  switch(e.key.keysym.sym){
	    case SDLK_ESCAPE:
	      done = true; break;
	      
	    case SDLK_r:
		updateSelector(true);
	      break;
	      
	    case SDLK_e:
		updateSelector(false);
		break;
	      
	    case SDLK_1:
		extrude();
	      break;
	      
	    case SDLK_UP:
	      _y = 0.f;
	      _x = 1.f;
	      _rotation -= 15.0f;
	      break;
	      
	    case SDLK_DOWN:
	      _y = 0.f;
	      _x = 1.f;
	      _rotation += 15.0f;
	      break;
	      
	      
	    case SDLK_LEFT:
	      _x = 0.f;
	      _y = 1.f;
	      _rotation -= 15.0f;
	      break;
	      
	    case SDLK_RIGHT:
	      _x = 0.f;
	      _y = 1.f;
	      _rotation += 15.0f;
	      break;
	      
	      
	      
	    default: break;
	  }
	  
	
      }
    }
    
  }
  
  void VBO::draw(){
//         glBindBuffer(GL_ARRAY_BUFFER, _positionVB);
//     glEnableClientState(GL_VERTEX_ARRAY);
//     glVertexPointer(3,GL_FLOAT,0,0);
// 
//     glBindBuffer(GL_ARRAY_BUFFER,_colorVB);
//     glEnableClientState(GL_COLOR_ARRAY);
//     glColorPointer(3,GL_FLOAT,0,0);
// 
//     glEnableClientState(GL_ELEMENT_ARRAY_BUFFER);
//     glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,_indexVB);
//     glDrawElements(GL_QUADS,24,GL_UNSIGNED_INT,0);
// 
//     glDisableClientState(GL_ELEMENT_ARRAY_BUFFER);
//     glDisableClientState(GL_VERTEX_ARRAY);
    
    
  glClearColor(1.f, 0.f, 1.f, 1.f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glLoadIdentity();
    glPushMatrix();
//     glTranslatef(1.f, 1.f, -16.f);
      glTranslatef(1.f, 1.f, -16.f);
    glRotatef(_rotation, _x, _y , 0.f);
    glLineWidth(5);
    glColor3f(1.f, 1.f, 0.f);
   render.at(0).draw();
   glColor3f(0.f, 0.f, 0.f);
   render.at(1).draw();

    glPopMatrix();
    SDL_GL_SwapBuffers();
  }
  
void VBO::updateSelector(bool more)
{
  
  if(more){
    indice_selector = indice_selector<max_selector?++indice_selector:0;
//     indice_selector == max_selector-1?0:indice_selector;
  }
  else
    indice_selector = indice_selector>0?--indice_selector:max_selector;
  //gambiarra
   Selector sel;
     sel.i[0] = 0;
     sel.i[1] = 1;
     sel.i[2] = 2;
     
     sel.v[0] = _m.getVertex(_m.getFace(indice_selector).getV1()).getPos();
     sel.v[1] = _m.getVertex(_m.getFace(indice_selector).getV2()).getPos();
     sel.v[2] = _m.getVertex(_m.getFace(indice_selector).getV3()).getPos();
     
     SelectorRender *tmp = (SelectorRender*) &render.at(1);
     tmp->recreate(sel);
}

void VBO::extrude()
{
  
  //calculate normal
  
  Vertex a = _m.getVertex(_m.getFace(indice_selector).getV1()) ;
  Vertex b = _m.getVertex(_m.getFace(indice_selector).getV2()) ;
  Vertex c = _m.getVertex(_m.getFace(indice_selector).getV3());
  
  Vec3 n1 = b.getPos() - a.getPos();
  Vec3 n2 = c.getPos() - a.getPos();
  
  Vec3 face_normal = n1.cross(n2);
  face_normal.print();
  face_normal=face_normal.normalize();
  
  Vertex newa, newb, newc;
  newa.setPos( a.getPos() + face_normal );
  newb.setPos(b.getPos() + face_normal );
  newc.setPos(c.getPos() + face_normal );
  
  
  
  
  Mesh sub_mesh;
  sub_mesh.conect(sub_mesh.addVertex(newa), 
		  sub_mesh.addVertex(newb),
		  sub_mesh.addVertex(newc)
 		);
  
  std::cout << "SIZE " << _m.faceSize() << std::endl;
  _m.conect(_m.addVertex(newa), 
		  _m.addVertex(newb),
		  _m.addVertex(newc));
  
    std::cout << "SIZE 2 " << _m.faceSize() << std::endl;
    Face tst = _m.getFace(_m.faceSize()-1);
    
    int tste[21] = {
      _m.getFace(indice_selector).getV1(), _m.getFace(indice_selector).getV2(), tst.getV1(),
       _m.getFace(indice_selector).getV2(), tst.getV1(), _m.getFace(indice_selector).getV3(),
          _m.getFace(indice_selector).getV2(), tst.getV2(), tst.getV1(),
           _m.getFace(indice_selector).getV3(), tst.getV1(), tst.getV3(),
                   _m.getFace(indice_selector).getV1(), _m.getFace(indice_selector).getV3(), tst.getV2(),
                           _m.getFace(indice_selector).getV1(),  tst.getV2(), tst.getV3(),
                           tst.getV2(), tst.getV1(), tst.getV3() 
    };
    
    max_selector = _m.faceSize()-1;
  
  TriangleMeshRender *t = (TriangleMeshRender*)& render.at(0);
  t->addSubMesh(sub_mesh, tste);

  
}

  
  void VBO::Vbo(){
    

//     _m = Obj::readObjToMesh("/home/edgar/Pictures/cavalo1.obj");
//     _m = Obj::readObjToMesh("/home/edgar/Downloads/Pikachu2.obj");
 //   _m = Obj::readObjToMesh("/home/edgar/Downloads/cube.obj");
 _m = Obj::readObjToMesh("/home/edgar/Downloads/boneco.obj");    
    
    Mesh _m2 ;
    
   
    
      
//       _m2.conect(_m2.addVertex(_m.getVertex(_m.getFace(0).getV1())),
// 	 _m2.addVertex(_m.getVertex(_m.getFace(0).getV2())),
// 		 _m2.addVertex(_m.getVertex(_m.getFace(0).getV3()))
//       );
    
    
    
//     TriangleMeshRender 
    
    max_selector = _m.faceSize()-1;
    indice_selector = 0;
    
     Selector sel;
     sel.i[0] = 0;
     sel.i[1] = 1;
     sel.i[2] = 2;
     
     sel.v[0] = _m.getVertex(_m.getFace(0).getV1()).getPos();
     sel.v[1] = _m.getVertex(_m.getFace(0).getV2()).getPos();
     sel.v[2] = _m.getVertex(_m.getFace(0).getV3()).getPos();
     
     
    SelectorRender selec(sel);
    TriangleMeshRender  tmesh(_m);
    render.push_back(tmesh);
    render.push_back(selec);
    tmesh.print();
    selec.print();
    
//       for(int i=0; i< _m.faceSize(); i++){
//       _m.getVertex(_m.getFace(i).getV1()).getPos().print();
//       _m.getVertex(_m.getFace(i).getV2()).getPos().print();
//       _m.getVertex(_m.getFace(i).getV3()).getPos().print();
//       
//       }
//     
// 
//     ind = _m.getArrayIndex();
//     ind2 = _m.getArrayPos();
//     
//     std::cout <<  "FACES: "  <<  ind.size() << std::endl 
// 		  <<   "VERTICES: " << ind2.size() << std::endl;
//     glGenBuffers(1, &_indexVB);
//     glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexVB);
//     glBufferData(GL_ELEMENT_ARRAY_BUFFER, ind.size() * sizeof(unsigned), ind.data(), GL_STATIC_DRAW);
//     
//     glGenBuffers(1, &_positionVB);
//     glBindBuffer(GL_ARRAY_BUFFER, _positionVB);
//     glBufferData(GL_ARRAY_BUFFER, ind2.size() * sizeof(float), ind2.data(), GL_STATIC_DRAW );
//     
  }
  

  
  
  
  
  
  
  
}
