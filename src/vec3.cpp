#include "vec3.h"
#include <iostream>
#include <cmath>

namespace uni{
Vec3::Vec3(){}

Vec3::Vec3(float x, float y, float z):_x(x),_y(y), _z(z){}

Vec3::~Vec3(){}

void Vec3::setX(float x){_x = x;}

void Vec3::setY(float y){_y = y;}

void Vec3::setZ(float z){_z = z;}

float Vec3::getX() const {return _x;}

float Vec3::getY() const {return _y;}

float Vec3::getZ() const {return _z;}

void Vec3::print() const{
  std::cout << "Vec3 ---  X: " << _x  << " Y: "
		<< _y << " Z :" << _z << std::endl;
}

Vec3 Vec3::operator+(const Vec3& other) const{
  return Vec3(
		      _x + other.getX(),
		      _y + other.getY(),
		      _z + other.getZ()
		    );
}

Vec3 Vec3::operator-(const Vec3& other) const{
  return Vec3(
		      _x - other.getX(),
		      _y - other.getY(),
		      _z - other.getZ()
		      );
}

Vec3 Vec3::operator*(float scalar) const{
  return Vec3(_x * scalar, _y * scalar, _z * scalar);
}

Vec3 Vec3::normalize() const{
    float module = fabs(sqrt(_x * _x + _y * _y + _z * _z));
    return Vec3(
                    _x / module,
                    _y / module,
                    _z / module
                );
}

Vec3 Vec3::cross(const Vec3& other) const{
    return Vec3(
		        _y * other.getZ() - _z * other.getY(),
			_z * other.getX() - _x * other.getZ(),
			_x * other.getY() - _y * other.getX()
		      );
}

// void Vec3::operator=(const Vec3& other) const
// {
//     setX(other.getX());
//     setY(other.getY());
//     setZ(other.getZ());
// }


}