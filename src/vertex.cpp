#include "vertex.h"

namespace uni{
  Vertex::Vertex(){}
  
  Vertex::~Vertex(){}
  
  Vec3 Vertex::getNormal(){
    return _normal;
  }
  
  Vec3 Vertex::getPos(){
    return _pos;
  }

  
void Vertex::setNormal(Vec3 normal)
{
  _normal = normal;
}

void Vertex::setPos(Vec3 pos)
{
  _pos = pos;
}

bool Vertex::operator==(const Vertex& other)
{
    return _pos.getX() == other._pos.getX()
	  && _pos.getY() == other._pos.getY()
	  && _pos.getZ() == other._pos.getZ()
	  && _normal.getX() == other._normal.getX()
	  && _normal.getY() == other._normal.getY()
	  && _normal.getZ() == other._normal.getZ();
}



}